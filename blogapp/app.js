var express = require("express"),
app = express(),
expSanitizer = require("express-sanitizer"),
bodyParser = require("body-parser"),
methoOverride = require("method-override"),
mongoose = require("mongoose");

//APP CONFIG
mongoose.connect("mongodb://localhost:27017/blog_app", {useNewUrlParser: true});
app.set("view engine", "ejs");
app.use(express.static("public"));
app.use(bodyParser.urlencoded({extended: true}));
app.use(expSanitizer());
app.use(methoOverride("_method"));

// MONGOOSE CONFIG
var blogSchema = new mongoose.Schema({
    title: String,
    image: String,
    body: String,
    created: {type: Date, default: Date.now()}
});
var Blog = mongoose.model("Blog", blogSchema);

// RESTFUL ROUTES
app.get("/", function(req, res){
   res.redirect("/blogs");
});

app.get("/blogs", function(req, res) {
    Blog.find({}, function(err, blogs){
       if(err){
           console.log("Something is wrong ", err);
       } else {
           res.render("index", {blogs: blogs});
       }
    });
});

// NEW ROUTE
app.get("/blogs/new", function(req, res) {
    res.render("new");
});

// CREATE ROUTE
app.post("/blogs", function(req, res) {
            // create new blog
            req.body.blog.body = req.santizie(req.body.blog.body);
        Blog.create(req.body.blog, function(err, newblog) {
            if(err){
                res.render("new");
            } else {
                // then, redirect back to blogs page
                res.redirect("/blogs");
            }
        });
});
// SHOW ROUTE
app.get("/blogs/:id", function(req, res) {
    Blog.findById(req.params.id, function(err, foundBlog){
        if(err){
            res.redirect("/blogs");
        } else {
            res.render("show", {blog: foundBlog});
        }
    })
});

// EDIT ROUTE 
app.get("/blogs/:id/edit", function(req, res) {
    Blog.findById(req.params.id, function(err, foundBlog) {
        if(err){
            res.redirect('/blogs');
        } else {
            res.render("edit", {blog: foundBlog})
        }
    }) 
});
// UPDATE ROUTE 
app.put("/blogs/:id", function(req,res) {
    req.body.blog.body = req.santizie(req.body.blog.body);
   Blog.findByOneAndUpdate(req.params.id, req.body.blog, function(err, updateBlog) {
       if(err){
           res.redirect("/blogs");
       } else {
               res.redirect("/blogs/" + req.params.id );
       }
   } );
});

app.delete("/blogs/:id", function(req, res) {
    Blog.findByIdAndDelete(req.params.id, function(err, deleteBlog){
       if(err){
           res.redirect("/blogs");
       } else {
           res.redirect("/blogs");
       }
    });
})

app.listen(process.env.PORT, process.env.IP, function() {
    console.log("server has started..");
});